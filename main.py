from flask import Flask, render_template, url_for, request, Response, json, redirect

app = Flask(__name__)

books = [{'title': 'software engineering', 'id': 1}, {
    'title': 'algo', 'id': 2}, {'title': 'ios', 'id': 3}]


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/book/')
def book():
    return render_template('book.html', books=books)


@app.route('/search/', methods=["GET", "POST"])
def bookSearch():
    if request.method == 'POST':
        search_key = request.form['name']
        return redirect(url_for('manybooks', nm=search_key))
    else:
        return render_template('index.html')


@app.route('/book/json/')
def bookjson():
    r = json.dumps(books)
    return Response(r)


if __name__ == '__main__':
    app.run()
